const mongoose = require('mongoose');

const VehiculoSchema = mongoose.Schema({
    nombre: String,
    placa: String,
    modelo: String
});

module.exports = mongoose.model("Vehiculo", VehiculoSchema);