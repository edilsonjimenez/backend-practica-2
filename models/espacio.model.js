const mongoose = require('mongoose');

const EspacioSchema = mongoose.Schema({
    numero: Number,
    tipo: String,
    disponible: {
        type: Boolean,
        default: true
    }
})

module.exports = mongoose.model('Espacio', EspacioSchema);