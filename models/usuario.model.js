const mongoose = require('mongoose');

const UsuarioSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true
    },
    apellido: {
        type: String,
        required: true
    },
    telefono: {
        type: String,
        required: true
    },
    dpi: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model("Usuario", UsuarioSchema);