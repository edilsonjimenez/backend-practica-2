const mongoose = require('mongoose');
//const { MongoMemoryServer } = require('mongodb-memory-server');
const supertest = require('supertest');
const app = require('../app');

const request = supertest(app);


var usuario;


const getUser = require('../__mocks__/axios').getUser;
const response = require('../tests/response');

const nock = require('nock');





describe("Pruebas de crud de usuario con mock", () => {
    beforeEach(() => {
        nock('https://mock.com')
          .get('/users/2924698510101')
          .reply(200, response);
      });
    
    
    
    it('Obtener usuario por dpi', () => {

       
        return getUser('2924698510101')
          .then(response => {

            expect(typeof response).toEqual('object');
            
            expect(response.dpi).toEqual("2924698510101");
            
          });
      });
    
    
});