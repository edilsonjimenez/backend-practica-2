const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');
const supertest = require('supertest');
const app = require('../app');
const request = supertest(app);

var vehiculo;

beforeAll(async () => {
    const opts = {};
    mongoServer = new MongoMemoryServer({ binary: { version: '4.2.8' } });
    const mongoUri = await mongoServer.getUri();
    await mongoose.connect(mongoUri, opts, (err) => {
        if (err) console.error(err);
    });

}, 600000);

afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
});

describe("Pruebas de crud de vehiculo", () => {
    it("verificacion de status en obtener todos los vehiculos", async done => {
        const res = await request.get("/vehiculos");

        expect(res.status).toEqual(200);
        done();
    });
    it("verificacion de status en crear un vehiculo sin datos", async done => {
        const res = await request.post("/vehiculos");

        expect(res.status).toEqual(400);
        done();
    });
    it("verificacion de status al editar un vehiculo sin enviar datos", async done => {
        const res = await request.put("/vehiculos/5f51c09c49eee3000aa2cb75");

        expect(res.status).toEqual(400);
        done();
    });
    it("verificacion de status al eliminar un vehiculo que no existe", async done => {
        const res = await request.delete("/vehiculos/5f51c09c49eee3000aa2cb75");

        expect(res.status).toEqual(404);
        done();
    });
    it("verificacion de status al obtener un vehiculo que no existe", async done => {
        const res = await request.get("/vehiculos/5f51c09c49eee3000aa2cb75");

        expect(res.status).toEqual(404);
        done();
    });

    it("Creacion de vehiculo", async done => {
        const res = await request.post("/vehiculos").send({
            nombre: "Mazda CX-5",
            placa: "GHT502",
            modelo: "2012"
        });

        vehiculo = res.body;

        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('id');
        expect(res.body).toHaveProperty('mensaje');
        expect(res.body.mensaje).toBe("Se ha creado el vehiculo con id: " + vehiculo.id)

        done();
    });
    it("verificacion de datos de vehiculo creado", async done => {
        const res = await request.get("/vehiculos/" + vehiculo.id);

        vehiculo = res.body;

        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('nombre');
        expect(res.body.nombre).toEqual('Mazda CX-5');
        expect(res.body).toHaveProperty('placa');
        expect(res.body.placa).toEqual('GHT502');
        expect(res.body).toHaveProperty('modelo');
        expect(res.body.modelo).toEqual('2012');

        done();
    });
    it("verificacion de vehiculo en todos los vehiculos", async done => {
        const res = await request.get("/vehiculos");

        expect(res.status).toEqual(200);
        expect([...res.body]).toContainEqual(vehiculo);
        done();
    });
    it("Edicion de datos un vehiculo", async done => {
        var res = await request.put("/vehiculos/" + vehiculo._id)
        .send({
            nombre: "Mazda CX-9",
            placa: "GHT503",
            modelo: "2013"
        });

        expect(res.status).toEqual(200);

        res = await request.get("/vehiculos/" + vehiculo._id);
        vehiculo = res.body;

        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('nombre');
        expect(res.body.nombre).toEqual("Mazda CX-9");
        expect(res.body).toHaveProperty('placa');
        expect(res.body.placa).toEqual("GHT503");
        expect(res.body).toHaveProperty('modelo');
        expect(res.body.modelo).toEqual("2013");

        done();
    });
    it("Eliminacion de vehiculo", async done => {
        const res = await request.delete("/vehiculos/" + vehiculo._id);

        expect(res.status).toEqual(200);
        done();
    });
    it("verificacion de status al editar un vehiculo sin enviar datos", async done => {
        var res = await request.put("/vehiculos/1234")
        .send({
            nombre: "Mazda CX-9",
            placa: "GHT503",
            modelo: "2013"
        });
        expect(res.status).toEqual(500);
        done();
    });
    it("verificacion de status al eliminar un vehiculo que no existe", async done => {
        const res = await request.delete("/vehiculos/1234");

        expect(res.status).toEqual(500);
        done();
    });
    it("verificacion de status al obtener un vehiculo que no existe", async done => {
        const res = await request.get("/vehiculos/1234");

        expect(res.status).toEqual(500);
        done();
    });
});