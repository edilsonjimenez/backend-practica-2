const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');
const supertest = require('supertest');
const app = require('../app');
const request = supertest(app);

var espacio;

beforeAll(async () => {
    const opts = {};
    mongoServer = new MongoMemoryServer({ binary: { version: '4.2.8' } });
    const mongoUri = await mongoServer.getUri();
    await mongoose.connect(mongoUri, opts, (err) => {
        if (err) console.error(err);
    });

}, 600000);

afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
});

describe("Pruebas unitarias crud de espacios de parqueo", () => {
    it("validacion de status al obtener todos los espacios", async done => {
        const res = await request.get("/espacios");

        expect(res.status).toEqual(200);
        expect(res.body).toEqual([]);
        done();
    });
    it("verificacion de status en crear un espacio sin datos", async done => {
        const res = await request.post("/espacios");

        expect(res.status).toEqual(400);
        done();
    });
    it("verificacion de status al editar un espacio sin enviar datos", async done => {
        const res = await request.put("/espacios/5f51c09c49eee3000aa2cb75");

        expect(res.status).toEqual(400);
        done();
    });
    it("verificacion de status al eliminar un espacio que no existe", async done => {
        const res = await request.delete("/espacios/5f51c09c49eee3000aa2cb75");

        expect(res.status).toEqual(404);
        done();
    });
    it("verificacion de status al obtener un espacio que no existe", async done => {
        const res = await request.get("/espacios/5f51c09c49eee3000aa2cb75");

        expect(res.status).toEqual(404);
        done();
    });
    it("Creacion de espacio", async done => {
        const res = await request.post("/espacios").send({
            numero: 1,
            tipo: "automovil",
            disponible: true
        });

        espacio = res.body;

        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('id');
        expect(res.body).toHaveProperty('mensaje');
        expect(res.body.mensaje).toBe("Se ha creado el espacio con id: " + espacio.id)

        done();
    });
    it("verificacion de datos de espacio creado", async done => {
        const res = await request.get("/espacios/" + espacio.id);

        espacio = res.body;

        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('numero');
        expect(res.body.numero).toEqual(1);
        expect(res.body).toHaveProperty('tipo');
        expect(res.body.tipo).toEqual('automovil');
        expect(res.body).toHaveProperty('disponible');
        expect(res.body.disponible).toEqual(true);

        done();
    });
    it("verificacion de espacio en todos los espacios", async done => {
        const res = await request.get("/espacios");

        expect(res.status).toEqual(200);
        expect([...res.body]).toContainEqual(espacio);
        done();
    });
    it("Edicion de datos un espacio", async done => {
        
        var res = await request.put("/espacios/" + espacio._id)
            .send({
                numero: 2,
                tipo: "motocicleta",
                disponible: false
            });

        expect(res.status).toEqual(200);

        res = await request.get("/espacios/" + espacio._id);
        espacio = res.body;

        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('numero');
        expect(res.body.numero).toEqual(2);
        expect(res.body).toHaveProperty('tipo');
        expect(res.body.tipo).toEqual("motocicleta");
        expect(res.body).toHaveProperty('disponible');
        expect(res.body.disponible).toEqual(false);

        done();
    });
    it("Eliminacion de espacio", async done => {
        const res = await request.delete("/espacios/" + espacio._id);

        expect(res.status).toEqual(200);
        done();
    });
    it("verificacion de status al editar un espacio con id incorrecto", async done => {
        var res = await request.put("/espacios/1234")
            .send({
                numero: 2,
                tipo: "motocicleta",
                disponible: false
            });
        expect(res.status).toEqual(500);
        done();
    });
    it("verificacion de status al eliminar un espacio con id incorrecto", async done => {
        const res = await request.delete("/espacios/1234");

        expect(res.status).toEqual(500);
        done();
    });
    it("verificacion de status al obtener un espacio con id incorrecto", async done => {
        const res = await request.get("/espacios/1234");

        expect(res.status).toEqual(500);
        done();
    });

});