const mongoose = require('mongoose');
//const { MongoMemoryServer } = require('mongodb-memory-server');
const supertest = require('supertest');
const app = require('../app');

const request = supertest(app);


var usuario;


beforeAll(async () => {
    const mongoose = require('mongoose');
    const host = 'cluster0.1ojwk.mongodb.net';
    const username = '2learn';
    const password = 'Nq5La6e3KZhZ8jTO';
    const database = 'tests';
    mongoose.set('useUnifiedTopology', true);
    mongoose.set('useNewUrlParser', true);
    await mongoose.connect(`mongodb+srv://${username}:${password}@${host}/${database}?retryWrites=true&w=majority`);



});


afterAll(async () => {
    await mongoose.disconnect();
    //await mongoServer.stop();
});

describe("Pruebas de crud de usuario", () => {
    // beforeEach(() => {
    //     nock('https://api.github.com')
    //       .get('/users/octocat')
    //       .reply(200, response);
    //   });
    
    it("verificacion del api para obtener usuarios", async done => {
        const res = await request.get("/usuarios");

        expect(res.status).toEqual(200);
        done();
    });

    it("Prueba de creacion correcta de usuario", async done => {
        const res = await request.post("/usuarios").send({
            nombre: "Juan",
            apellido: "Gonzalez",
            telefono: "44556677",
            dpi: "2789584210101"
        });

        usuario = res.body;
        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('id');
        expect(res.body).toHaveProperty('mensaje');
        expect(res.body.mensaje).toBe("El usuario se ha creado correctamente")

        done();
    });

    it("validacion de usuario encontrado", async done => {
        const res = await request.get("/usuarios/" +  usuario.id);
        //usuario = res.body;
        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('nombre');
        expect(res.body).toHaveProperty('apellido');
        expect(res.body).toHaveProperty('telefono');
        expect(res.body).toHaveProperty('dpi');
        done();
    });

    it("validacion de error al obteber usuario", async done => {
        const res = await request.get("/usuarios/" +  "4546546asdfasdf000cvvfxg");
        //usuario = res.body;
        expect(res.status).toEqual(500);
       
        done();
    });

    it("Se valida que el dpi ya fue registrado", async done => {
        var res = await request.post("/usuarios").send({
            nombre: "Juan",
            apellido: "González",
            telefono: "44556677",
            dpi: "2789584210101"
        });

        expect(res.status).toEqual(400);
        expect(res.body).toHaveProperty('mensaje');
        expect(res.body.mensaje).toEqual('El dpi ya fue registrado');

        done();
    });

    it("Se valida que se enviaron campos vacios", async done => {
        var res = await request.post("/usuarios").send({
            nombre: "",
            apellido: "González",
            telefono: "44556677",
            dpi: "2789584730101"
        });    
        expect(res.status).toEqual(400);
        expect(res.body).toHaveProperty('errors');
        const { errors } = res.body;
        const noErrors = res.body.errors.length;
        for (var i = 0; i < noErrors; i++) {

            switch (errors[i].param) {
                case "nombre":
                    expect(errors[i].msg).toEqual('El nombre no puede estar vacio');
                    break;
                case "apellido":
                    expect(errors[i].msg).toEqual('El apellido no puede estar vacio');
                    break;
                case "telefono":
                    expect(errors[i].msg).toEqual('El telefono no puede estar vacio');
                    break;
                case "dpi":
                    expect(errors[i].msg).toEqual('El dpi no puede estar vacio');
                    break;
                default:
                    break;
            }
        }
        done();
    });

    it("validacion de edicion erronea de usuario ", async done => {
        const res = await request.put("/usuarios/5f66afc0365cc137d00975cf");

        expect(res.status).toEqual(400);
        done();
    });

    it("Prueba de Editar Usuario", async done => {
        var res = await request.put("/usuarios/" +  usuario.id )
        .send({
            nombre: "Julio",
            apellido: "Gonzalez",
            telefono: "44556677",
            dpi: "2789548670101"
        });

        expect(res.status).toEqual(200);

        res = await request.get("/usuarios/" +  usuario.id);
        //usuario = res.body;

        expect(res.status).toEqual(200);
        expect(res.body.nombre).toEqual("Julio");
        expect(res.body.apellido).toEqual("Gonzalez");
        expect(res.body.telefono).toEqual("44556677");
        expect(res.body.dpi).toEqual("2789548670101");

        done();
    });

    

    it("validacion de campos al obtener usuario", async done => {
        const res = await request.get("/usuarios/" +  usuario.id);
        
        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('nombre');
        expect(res.body).toHaveProperty('apellido');
        expect(res.body).toHaveProperty('telefono');
        expect(res.body).toHaveProperty('dpi');
        done();
    });

    it("validacion de eliminar usuario correctamente", async done => {
        const res = await request.delete("/usuarios/" +  usuario.id );

        expect(res.status).toEqual(200);
        done();
    });

    it("validacion de eliminar usuario que no existe", async done => {
        const res = await request.delete("/usuarios/" +  "5f66d605e818c8188857e44d" );

        expect(res.status).toEqual(404);
        done();
    });

    it("validacion de error al eliminar usuario que no existe", async done => {
        const res = await request.delete("/usuarios/" +  "5f66d605e818c8188857e44dzzzsdf45646" );

        expect(res.status).toEqual(500);
        done();
    });

    it("validacion de usuario no encontrado", async done => {
        const res = await request.get("/usuarios/5f66d605e818c8188857e74d");
        usuario = res.body;
        expect(res.status).toEqual(404);
        
        done();
    });
    
});