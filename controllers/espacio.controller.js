const Espacio = require('../models/espacio.model');

module.exports = {
    obtenerEspacios: (req, res) => {
        Espacio.find()
            .then(espacios => {
                return res.send(espacios);
            })
            .catch(err => {
                // console.log(err);
                return res.status(500).send({
                    mensaje: "Ocurrio un error al obtener los espacios"
                })
            })
    },
    obtenerEspacio: (req, res) => {
        const { id } = req.params;
        Espacio.findById(id)
            .then(espacio => {
                if (!espacio) {
                    return res.status(404).send({
                        mensaje: "No existe espacio con id: " + id
                    })
                }

                return res.send(espacio);
            })
            .catch(err => {
                // console.log(err);
                return res.status(500).send({
                    mensaje: "Ocurrio un error al obtener el espacio con id: " + id
                })
            })
    },
    crearEspacio: (req, res) => {
        const { numero, tipo, disponible } = req.body;
        if (!numero || !tipo || disponible === undefined) {
            return res.status(400).mensaje({
                mensaje: 'Todos los campos son obligatorios'
            });
        }

        Espacio.create({
            numero,
            tipo,
            disponible
        })
            .then(espacio => {
                return res.send({
                    id: espacio._id,
                    mensaje: "Se ha creado el espacio con id: " + espacio.id
                });
            })
            .catch(err => {
                return res.status(500).send({
                    mensaje: "Ocurrio un error al crear el espacio"
                })
            })
    },
    editarEspacio: (req, res) => {
        const { id } = req.params;
        const { numero, tipo, disponible } = req.body;
        
        if (!numero || !tipo || disponible === undefined) {
            return res.status(400).mensaje({
                mensaje: 'Todos los campos son obligatorios'
            });
        }

        Espacio.findByIdAndUpdate(id, {
            numero,
            tipo,
            disponible
        })
            .then(espacio => {
                if (!espacio) {
                    return res.status(404).send({
                        mensaje: "No existe espacio con id: " + id
                    });
                }
                return res.send(espacio);
            })
            .catch(err => {
                return res.status(500).send({
                    mensaje: "Ocurrio un error al editar el espacio con id: " + id
                })
            })
    },
    eliminarEspacio: (req, res) => {
        const { id } = req.params;

        Espacio.findByIdAndRemove(id)
            .then(espacio => {
                if (!espacio) {
                    return res.status(404).send({
                        mensaje: "No existe espacio con id: " + id
                    });
                }
                return res.send({
                    mensaje: "Se ha eliminado el espacio con id: " + id
                });
            })
            .catch(err => {
                return res.status(500).send({
                    mensaje: "Ocurrio un error al editar el espacio con id: " + id
                })
            })
    }
}