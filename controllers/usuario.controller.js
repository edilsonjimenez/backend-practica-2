const UsuarioModel = require('../models/usuario.model');
const { check, validationResult } = require('express-validator');


module.exports = {
    crearUsuario: (req, res) => {
        const errors = validationResult(req);       
  
    if (!errors.isEmpty()) { // se verifica que no existan errores de validacion
        return res.status(400).send(errors);
    }     
    else { 
        
        const { nombre } = req.body;
        const { apellido } = req.body;
        const { telefono } = req.body;
        const { dpi } = req.body;
        
        UsuarioModel.find({ dpi: dpi }, function (err, result) {
            console.log(err);
            if (err) throw err;
            if (result.length > 0) {
                return res.status(400).send({
                    mensaje: 'El dpi ya fue registrado'
                });
            } else {
               
                    UsuarioModel.create({ nombre, apellido, telefono, dpi})
                        .then(usuario => {
                         
                            if (usuario) {
                                return res.status(200).send({
                                    mensaje: 'El usuario se ha creado correctamente',
                                    id: usuario._id
                                })
                            }
                        })
                        .catch(err => {
                            console.log(err);
                            return res.status(500).send({
                                mensaje: 'Ha ocurrido un error creando el Usuario'
                            });
                        });
                }
            })

               
        }

    },

    

    obtenerUsuarios: (req, res) => {
        UsuarioModel.find().then(usuarios => {
                        
            return res.send(usuarios);
            
        })
            .catch(err => {
                return res.status(500).send({
                    mensaje: 'Error al obtener usuarios.'
                });
            });
    },

    obtenerUsuario: (req, res) => {
        const { id } = req.params;
        UsuarioModel.findById(id)
            .then(usuario => {
                if(usuario) {
                    return res.send(usuario);
                }else{
                    return res.status(404).send({
                        mensaje: "No existe ningun usuario con id: " + id
                    })
                }
            })
            .catch(err => {
                
                return res.status(500).send({
                    mensaje: "Ocurrio un error al obtener el usuario con id: " + id
                })
            })
    },

    editarUsuario: (req, res) => {
        const { id } = req.params;
        const { nombre } = req.body;
        const { apellido } = req.body;
        const { telefono } = req.body;
        const { dpi } = req.body;
        
        if(!nombre || !apellido || !telefono || !dpi){
            return res.status(400).mensaje({
                mensaje: 'Todos los campos son obligatorios'
            });
        }

        UsuarioModel.findByIdAndUpdate(id, {
            nombre,
            apellido,
            telefono,
            dpi
        })
            .then(usuario => {
                if (!usuario) {
                    return res.status(404).send({
                        mensaje: "No existe usuario con id: " + id
                    });
                }
                return res.send(usuario);
            })
            .catch(err => {
                return res.status(500).send({
                    mensaje: "Ocurrio un error al editar el usuario con id: " + id
                })
            })
    },

    eliminarUsuario: (req, res) => {
        const { id } = req.params;

        UsuarioModel.findByIdAndRemove(id)
            .then(usuario => {
                if (!usuario) {
                    return res.status(404).send({
                        mensaje: "No existe usuario con id: " + id
                    });
                }
                return res.send({
                    mensaje: "Se ha eliminado el usuario con id: " + id
                });
            })
            .catch(err => {
                return res.status(500).send({
                    mensaje: "Ocurrio un error al eliminar el usuario con id: " + id
                })
            })
    }

    
    
}
