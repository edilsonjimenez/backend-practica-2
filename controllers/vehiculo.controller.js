const Vehiculo = require('../models/vehiculo.model');

module.exports = {
    obtenerVehiculos: (req, res) => {
        Vehiculo.find()
            .then(vehiculos => {
                return res.send(vehiculos);
            })
            .catch(err => {
                // console.log(err);
                return res.status(500).send({
                    mensaje: "Ocurrio un error al obtener los vehiculos"
                })
            })
    },
    obtenerVehiculo: (req, res) => {
        const { id } = req.params;
        Vehiculo.findById(id)
            .then(vehiculo => {
                if (!vehiculo) {
                    return res.status(404).send({
                        mensaje: "No existe vehiculo con id: " + id
                    })
                }

                return res.send(vehiculo);
            })
            .catch(err => {
                // console.log(err);
                return res.status(500).send({
                    mensaje: "Ocurrio un error al obtener el vehiculo con id: " + id
                })
            })
    },
    crearVehiculo: (req, res) => {
        const { nombre, placa, modelo } = req.body;
        if(!nombre || !placa || !modelo){
            return res.status(400).mensaje({
                mensaje: 'Todos los campos son obligatorios'
            });
        }

        Vehiculo.create({
            nombre,
            placa,
            modelo
        })
            .then(vehiculo => {
                return res.send({
                    id: vehiculo._id,
                    mensaje: "Se ha creado el vehiculo con id: " + vehiculo.id
                });
            })
            .catch(err => {
                return res.status(500).send({
                    mensaje: "Ocurrio un error al crear el vehiculo"
                })
            })
    },
    editarVehiculo: (req, res) => {
        const { id } = req.params;
        const { nombre, placa, modelo } = req.body;

        if(!nombre || !placa || !modelo){
            return res.status(400).mensaje({
                mensaje: 'Todos los campos son obligatorios'
            });
        }

        Vehiculo.findByIdAndUpdate(id, {
            nombre,
            placa,
            modelo
        })
            .then(vehiculo => {
                if (!vehiculo) {
                    return res.status(404).send({
                        mensaje: "No existe vehiculo con id: " + id
                    });
                }
                return res.send(vehiculo);
            })
            .catch(err => {
                return res.status(500).send({
                    mensaje: "Ocurrio un error al editar el vehiculo con id: " + id
                })
            })
    },
    eliminarVehiculo: (req, res) => {
        const { id } = req.params;

        Vehiculo.findByIdAndRemove(id)
            .then(vehiculo => {
                if (!vehiculo) {
                    return res.status(404).send({
                        mensaje: "No existe vehiculo con id: " + id
                    });
                }
                return res.send({
                    mensaje: "Se ha eliminado el vehiculo con id: " + id
                });
            })
            .catch(err => {
                return res.status(500).send({
                    mensaje: "Ocurrio un error al editar el vehiculo con id: " + id
                })
            })
    }
}