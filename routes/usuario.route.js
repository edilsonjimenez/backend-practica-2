const { check, validationResult }  = require('express-validator'); 

const UsuarioController = require('../controllers/usuario.controller');

module.exports = (app) => {
    app.get('/usuarios', UsuarioController.obtenerUsuarios);
    app.get('/usuarios/:id', UsuarioController.obtenerUsuario);
    app.put('/usuarios/:id', UsuarioController.editarUsuario);
    app.delete('/usuarios/:id', UsuarioController.eliminarUsuario);
    app.post('/usuarios',     
    [check('nombre', 'El nombre no puede estar vacio').exists().isLength({ min: 1 }),
    check('apellido', 'El apellido no puede estar vacio').exists().isLength({ min: 1 }),
    check('telefono', 'El telefono no puede estar vacio').exists().isLength({ min: 1 }),
    check('dpi', 'El dpi no puede estar vacio').exists().isLength({ min: 1 })
    ], 
    UsuarioController.crearUsuario);
}