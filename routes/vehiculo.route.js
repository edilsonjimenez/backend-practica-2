const VehiculoController = require('../controllers/vehiculo.controller');

module.exports = (app) => {
    app.get('/vehiculos', VehiculoController.obtenerVehiculos);
    app.get('/vehiculos/:id', VehiculoController.obtenerVehiculo);
    app.post('/vehiculos', VehiculoController.crearVehiculo);
    app.put('/vehiculos/:id', VehiculoController.editarVehiculo);
    app.delete('/vehiculos/:id', VehiculoController.eliminarVehiculo);
}