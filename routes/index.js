module.exports = (app, server) => {
    require('./vehiculo.route')(app);
    require('./usuario.route')(app);
    require('./espacio.route')(app);
}