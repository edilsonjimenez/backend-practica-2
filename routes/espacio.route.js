const EspacioController = require('../controllers/espacio.controller');

module.exports = (app) => {
    app.get('/espacios', EspacioController.obtenerEspacios);
    app.get('/espacios/:id', EspacioController.obtenerEspacio);
    app.post('/espacios', EspacioController.crearEspacio);
    app.put('/espacios/:id', EspacioController.editarEspacio);
    app.delete('/espacios/:id', EspacioController.eliminarEspacio);
}