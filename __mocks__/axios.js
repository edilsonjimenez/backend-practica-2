const axios = require('axios');

module.exports = {
  getUser(dpi) {
    return axios
      .get(`https://mock.com/users/${dpi}`)
      .then(res => res.data)
      .catch(error => console.log(error));
  }
};